package cl.dci.ufro.demomvapp.Controller;

import org.jsoup.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")
public class Funcionalidad2 {
    @GetMapping("/funcionalidad_2")
    public String getVariacionPorcentual(@RequestParam(value="dia")String dia,@RequestParam(value="mes")String mes) throws IOException {
        String url = "https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=";
        double valorviejo=0;
        double valorseleccionado=1212.0;
        double variacionporcentual=0;
        int auxdia;
        auxdia=Integer.parseInt(dia);
        if (auxdia<10){
            dia="0"+auxdia;
        }
        else if (auxdia<31){
            auxdia=auxdia+1;
            dia=String.valueOf(auxdia);
        }
        else{
            return "error "+dia+ "no existe";
        }
        Document doc = Jsoup.connect(url).get();
        Element ufviejo = doc.getElementById("gr_ctl02_Enero");
        String auxviejo=ufviejo.text();
        auxviejo = auxviejo.replaceAll("(?:(?!\\d|Free)(?s:.))*(\\d+(?:[,]\\d+)*|Free)?", "$1");
        auxviejo=auxviejo.replaceAll(",",".");
        valorviejo=Double.parseDouble(auxviejo);

        Element ufseleccionado = doc.getElementById("gr_ctl"+dia+"_"+mes);
        String auxselccionado;
        auxselccionado=ufseleccionado.text();
        auxselccionado = auxselccionado.replaceAll("(?:(?!\\d|Free)(?s:.))*(\\d+(?:[,]\\d+)*|Free)?", "$1");
        auxselccionado=auxselccionado.replaceAll(",",".");
        valorseleccionado=Double.parseDouble(auxselccionado);
        variacionporcentual=((valorseleccionado-valorviejo)/valorviejo)*100;
        return "la variacion porcentual es la siguiente: " + variacionporcentual+"%";
    }

    }
