package cl.dci.ufro.demomvapp.Controller;

import org.jsoup.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.Locale;

@RestController
@RequestMapping(value = "/api")

public class Funcionalidad1 {

    @GetMapping(value = "/funcionalidad_1")
    public String funcionalidad1() throws IOException {
        String url = "https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=";
        Document doc = Jsoup.connect(url).get();
        Month mesAhora = LocalDate.now().getMonth();
        int diaHoy = LocalDate.now().getDayOfMonth()+1;
        String dia = String.valueOf(diaHoy);
        if(diaHoy < 10){
            dia = "0"+dia;
        }
        String mesOriginal = mesAhora.getDisplayName(TextStyle.FULL, new Locale("es","ES"));
        String output = upperCaseFirst(mesOriginal);
        String span = "gr_ctl"+dia+"_"+output;
        Element idDiv = doc.getElementById(span);
        String s = idDiv.text();
        return s;
    }

    public String upperCaseFirst(String v){
        char[] arreglo = v.toCharArray();
        arreglo[0] = Character.toUpperCase(arreglo[0]);
        return new String(arreglo);
    }
}